#!/usr/bin/env groovy
import pipeline.JenkinsPipelineModel

/**
 * Defines the global jenkins pipeline model.
 * @param body The body that should be executed.
 */
def call(body) {
  Map globals = new HashMap()
  try {
    originalOwner = body.owner
    model = new JenkinsPipelineModel()
    body.delegate = model
    body.resolveStrategy = Closure.DELEGATE_ONLY
    body()
    globals = [
      jenkins          : originalOwner,
      env              : env,
      steps            : steps,
      currentBuild     : currentBuild,
      scm              : scm,
      docker           : docker,
      params           : params,
      rocketChannel    : '',
      stopBuildPipeline: false
    ]
    model.execute(globals)
  } catch (err) {
    try {
      if(globals['rocketChannel']) {
        rocketSend channel: globals['rocketChannel'], message: "Build failed: ${err} - ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)"
      }
      rocketSend "Build failed: ${err} - ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)"
      currentBuild.result = 'FAILURE'
      throw err
    } catch (ignored) {
      // ignore send error from rocketSend
    }
  }
}
