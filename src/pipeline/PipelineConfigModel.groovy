package pipeline

/**
 * Model, which configures the pipeline and is passed between each step.
 */
@SuppressWarnings('unused')
class PipelineConfigModel {

  String dockerRegistry
  String dockerRegistryCredentialsId
  boolean executed = false
  boolean isCleanUpRequested = false
  Map globals
  List jobParameters = []
  String mavenSettingsId
  String rocketChannel = ''

  // Pipeline settings for the logRotator within the buildDiscarder
  // Set default values to be consistent with the previous implementation
  String numToKeepStr = '5'
  String daysToKeepStr = ''
  List pipelineJobBaseNameForChoiceParameter = []

  /**
   * The docker registry.
   * @param registry The registry.
   * @param credentialsId The id of the credentials - like they are referenced in Jenkins configuration.
   */
  def dockerRegistry(registry, credentialsId = null) {
    dockerRegistry = registry
    dockerRegistryCredentialsId = credentialsId
  }

  /**
   * Defines a reference to the maven settings.
   * @param settingsId The id of the settings - like they are referenced in Jenkins configuration.
   */
  def mavenSettings(settingsId) {
    mavenSettingsId = settingsId
  }

  /**
   * Defines whether the build requested a clean up
   * @param requestCleanup
   * @return
   */
  def shouldCleanUp(requestCleanup) {
    this.isCleanUpRequested = requestCleanup
  }

  /**
   * Define rocketchat channel for failing build messages.
   * @param channel
   * @return
   */
  def buildFailedRocketChannel(channel) {
    this.rocketChannel = channel
  }

  /**
   * Adds a parameter with a boolean default value.
   * @param name The name of the parameter.
   * @param description The description of the parameter.
   * @param defaultValue The default value of the parameter.
   */
  def booleanParameter(String name, String description = '', boolean defaultValue = false) {
    jobParameters.add([
      $class      : 'BooleanParameterDefinition',
      name        : name,
      description : description,
      defaultValue: defaultValue
    ])
    updateProperties()
  }

  /**
   * Adds a parameter with a string default value.
   * @param name The name of the parameter.
   * @param description The description of the parameter.
   * @param defaultValue The default value of the parameter.
   */
  def stringParameter(String name, String description = '', String defaultValue = false) {
    jobParameters.add([
      $class      : 'StringParameterDefinition',
      name        : name,
      description : description,
      defaultValue: defaultValue
    ])
    updateProperties()
  }

  /**
   * Add a select box as parameter to deployment
   * @param name The name of the parameter.
   * @param description The description of the parameter.
   * @param choices List of selectable items
   */
  def choiceParameter(String name, String description, List<String> choices) {
    jobParameters.add([
      $class     : 'ChoiceParameterDefinition',
      choices    : choices.join("\n"),
      name       : name,
      description: description
    ])
    updateProperties()
  }

  /**
   * Remove a parameter from the job parameter list.
   * @param name of the job parameter to be removed.
   */
  def removeParameter(String name) {
    List objectsToBeRemoved = []
    jobParameters.each { o ->
      if (o.name && o.name == name) {
        objectsToBeRemoved.add(o)
      }
    }
    objectsToBeRemoved.each { o -> jobParameters.remove(o) }
    updateProperties()
  }

  /**
   * Sets the log-rotation for the build-discarder how many builds are kept at most.
   * @param maxBuildsToKeep The amount of builds to keep.
   */
  def logRotationMaxBuildsToKeep(int maxBuildsToKeep) {
    if (maxBuildsToKeep > 0) {
      this.numToKeepStr = maxBuildsToKeep.toString()
    }
  }

  /**
   * Sets the log-rotation for the build-discarder when to delete old builds.
   * @param daysToKeepBuilds Number of days that builds are kept.
   */
  def logRotationDaysToKeepBuilds(int daysToKeepBuilds) {
    if (daysToKeepBuilds > 0) {
      this.daysToKeepStr = daysToKeepBuilds.toString()
    }
  }

  /**
   * Add a choice parameter with the pipeline job base name as entry.
   * @param parameterName Name of the config parameter
   * @param description A description of the config parameter
   * @param position The position to orderly insert this parameter in the jobParameters list
   * @return
   */
  def usePipelineJobBaseNameForChoiceParameter(String parameterName, String description, int position){
    this.pipelineJobBaseNameForChoiceParameter.add([
            parameterName : parameterName,
            description: description,
            position: position
    ])
  }

  /**
   * Does the execution.
   * @param globals The global variables map.
   */
  def execute(Map globals) {
    globals['rocketChannel'] = this.rocketChannel
    this.globals = globals
    this.shouldCleanUp(isCleanUpRequested)
    doExecute()
  }

  /**
   * Execution sets some job properties.
   * This is called once when the pipeline is set up. If a step adds/changes some build options later, it's re-executed
   *
   * <p>
   *   Note: The jobParameters will be made accessible via the environment (env.<parameterName>) by the Multibranch plugin.
   * </p>
   * <p>
   *   See: <a href="https://www.jenkins.io/doc/pipeline/steps/workflow-multibranch/#properties-set-job-properties">workflow-multibranch/#properties-set-job-properties</a> subsection "parameters"
   * </p>
   *
   */
  def doExecute() {
    def steps = this.globals.steps

    if(!this.pipelineJobBaseNameForChoiceParameter.isEmpty()){
      def pipelineJobBaseName = this.globals.env.JOB_BASE_NAME
      this.pipelineJobBaseNameForChoiceParameter.forEach { entry ->
          this.jobParameters.add(entry.position - 1, [
                  $class     : 'ChoiceParameterDefinition',
                  choices    : List.of(pipelineJobBaseName),
                  name       : entry.parameterName,
                  description: entry.description
          ])
      }
    }

    if (this.jobParameters.isEmpty()) { // avoid the build to be "parameterized" when no params are set
      steps.properties properties: [
        steps.disableConcurrentBuilds(),
        steps.buildDiscarder(steps.logRotator(numToKeepStr: this.numToKeepStr, daysToKeepStr: this.daysToKeepStr)),
        steps.disableResume()
      ]
    } else {
      steps.properties properties: [
        steps.disableConcurrentBuilds(),
        steps.buildDiscarder(steps.logRotator(numToKeepStr: this.numToKeepStr, daysToKeepStr: this.daysToKeepStr)),
        steps.disableResume(),
        steps.parameters(this.jobParameters)
      ]
    }
    this.executed = true
  }

  /**
   * This runs doExecute but only when the properties were already set
   */
  def updateProperties() {
    if (this.executed) {
      doExecute()
    }
  }


}
