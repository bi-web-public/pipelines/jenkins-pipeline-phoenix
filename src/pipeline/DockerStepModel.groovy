package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute docker builds and docker tag relabeling.
 */
@InheritConstructors
@SuppressWarnings('unused')
class DockerStepModel extends AbstractStepModel {

  boolean skipStepExecution = false
  String imageName = null
  String renameFromTag = null
  String renameToTag = null
  ArrayList<String> dockerTags = new ArrayList<>()
  String buildArgs = ""
  String executionDirectory = null

  String dockerRegistry
  String dockerRegistryCredentialsId

  /**
   * Overwrite the docker registry set in pipeline config.
   * @param registry The registry.
   * @param credentialsId The id of the credentials - like they are referenced in Jenkins configuration.
   */
  def overwriteDockerRegistry(registry, credentialsId = null) {
    dockerRegistry = registry
    dockerRegistryCredentialsId = credentialsId
  }

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Sets name name of the docker image.
   * @param imageName The name of the docker image.
   */
  void dockerImageName(String imageName) {
    this.imageName = imageName
  }

  /**
   * Adds a tag, which is used to tag the build docker image.
   * @param tagToAdd The tag to add.
   */
  void addDockerTag(String tagToAdd) {
    if (tagToAdd != null) {
      tagToAdd = tagToAdd.replace("/", "-")
    }
    dockerTags.push(tagToAdd)
  }

  /**
   * Adds build arguments to the docker image build command.
   * @param buildArgToAdd The build-arg to add.
   */
  void addBuildArg(String buildArgToAdd) {
    if (buildArgToAdd != null && !buildArgToAdd.isBlank()) {
      this.buildArgs += " --build-arg ${buildArgToAdd} "
    }
  }

  /**
   * Pulls an image with an given tag and pushes it with the new tag.
   * The relabeling is done on step execution.
   * @param renameFromTag The old tag.
   * @param renameToTag The new tag
   */
  void dockerReTag(String renameFromTag, String renameToTag) {
    if (renameFromTag == null || renameFromTag.length() == 0) {
      steps.error("Docker rename action triggered but 'renameFromTag' not set!")
    }
    if (renameToTag == null || renameToTag.length() == 0) {
      steps.error("Docker rename action triggered but 'renameToTag' not set!")
    }
    this.renameFromTag = renameFromTag
    this.renameToTag = renameToTag
  }

  /**
   * Sets the directory for the docker step execution.
   * If no directory is set, all docker actions are executed in root
   * directory of the repository.
   * @param directory The directory.
   */
  void dir(String directory) {
    this.executionDirectory = directory
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def imageName = this.imageName

    def body = {

      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Starting Docker build")

      //allow manipulation of imageName inside before{}
      boolean combineImageNameParts = false
      if (!this.imageName || this.imageName.length() == 0) combineImageNameParts = true

      runBeforeScripts(config, globals)
      steps.echo("--- after runBeforeScripts: (repo) imageName=${imageName}")
      steps.echo("--- after runBeforeScripts: (image) this.imageName=${this.imageName}")

      if (combineImageNameParts) imageName = "${imageName}/${this.imageName}"


      if (!skipStepExecution) {
        if (!imageName) {
          steps.error("Docker imageName is not set!")
        }
        if (dockerTags.size() > 0) {
          steps.echo("--- docker build  ${buildArgs} -t ${imageName}:${String.join(" -t ${imageName}:", dockerTags)} .")
          steps.sh("podman build ${buildArgs} -t ${imageName}:${String.join(" -t ${imageName}:", dockerTags)} .")
          for (String tag in dockerTags) {
            steps.echo("Pushing docker image '${imageName}' with tag '${tag}'")
            steps.sh("podman push ${imageName}:${tag}")
          }
        }
      }
      if (renameFromTag && renameToTag) {
        steps.echo("Renaming docker tag from ${imageName}:${renameFromTag} to ${imageName}:${renameToTag}")
        steps.echo("Preparing rename operation by pulling:  ${imageName}:${renameFromTag}")
        steps.sh("podman pull ${imageName}:${renameFromTag}")
        steps.sh("podman tag ${imageName}:${renameFromTag} ${imageName}:${renameToTag}")
        steps.sh("podman push ${imageName}:${renameToTag}")
      }
      runAfterScripts(config, globals)
    }

    // Changes the execution directory before step execution.
    if (executionDirectory) {
      def innerBody = body
      body = {
        steps.dir(executionDirectory) {
          innerBody()
        }
      }
    }

    // if docker registry is configured, wrap this in "docker.withRegistry"
    // and add the registry to the image name
    def registry
    def registryCredentials
    if (this.dockerRegistry) {
      // use the registry set by overwriteDockerRegistry()
      registry = this.dockerRegistry
      registryCredentials = this.dockerRegistryCredentialsId
    } else if (config.dockerRegistry) {
      // use the registry set by the PipelineConfigModel
      registry = config.dockerRegistry
      registryCredentials = config.dockerRegistryCredentialsId
    }

    if (registry) {
      steps.echo("--- dockerRegistry is ${registry}")
      //handle situation if imageName is later set via build Parameter
      if (imageName)
        imageName = "${registry}/${imageName}"
      else
        imageName = registry

      steps.echo("--- imageName is ${imageName} (set within if-clause, the image name might by extended by build parameters) ")
      def innerBody = body
      body = {
        globals.docker.withRegistry("https://${registry}", registryCredentials) {
          innerBody()
        }
      }
    } else {
      steps.error("No Docker registry defined!")
    }

    steps.stage('Docker Build') {
      steps.container(stepContainerName()) {
        body()
      }
    }
  }

  @Override
  String stepContainerName() {
    return "docker"
  }

  @Override
  String stepContainerImageName(config) {
    return "mgoltzsche/podman:latest"
  }

  @Override
  def createContainerTemplate(config, steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(config),
      privileged: true,
      command: 'tail -f /dev/null',
      alwaysPullImage: false
    )
  }
}
