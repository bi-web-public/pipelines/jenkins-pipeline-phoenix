package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to download maven artifacts.
 * <p>
 * This step exposes the environment variable "BI_MAVEN_DEPENDENCY_COPY_INFO" containing a list of the downloaded artifacts.<br/>
 * BI_MAVEN_DEPENDENCY_COPY_INFO=[&lt;key1&gt;=&lt;value1&gt;[,&lt;key2&gt;=&lt;value2&gt;[...]]] <br/>
 * where &lt;key&gt; is the filename or an otherwise specified name <br/>
 * and &lt;value&gt; is the maven artifact in the form "groupId:artifactId:version[:packaging]"
 * <p/>
 */
@InheritConstructors
@SuppressWarnings('unused')
class MavenDependencyCopyStepModel extends AbstractStepModel {

  boolean skipStepExecution = false
  def mavenLogLevel = "info"
  Map<String, String> mavenArtifacts = [:]
  def sourceDirectory = ""
  def outputDirectory = "./dependencies"

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Overwrite the default maven log level. <br/>
   * Must be one of ("trace", "debug", "info", "warn", "error" or "off")
   *
   * @param mavenLogLevel The log level.
   */
  void setMavenLogLevel(String mavenLogLevel) {
    if (mavenLogLevel != null && !mavenLogLevel.isBlank()) {
      this.mavenLogLevel = mavenLogLevel.trim()
    }
  }

  /**
   * Download the specified maven artifact.
   *
   * @param name A unique name used as key in the environment variable .
   * @param mavenArtifact The artifact in the form "groupId:artifactId:version[:packaging]"
   */
  void downloadArtifact(String name, String mavenArtifact) {
    if (name != null && !name.isBlank()) {
      if (mavenArtifact != null && !mavenArtifact.isBlank()) {
        this.mavenArtifacts.put(name.trim(), mavenArtifact.trim())
      }
    }
  }

  /**
   * Download maven artifacts by reading POM files in the specified directory.
   *
   * @param sourceDirectory A location containing POM files.
   */
  void downloadArtifactsUsingPomFiles(String sourceDirectory) {
    if (sourceDirectory != null && !sourceDirectory.isBlank()) {
      this.sourceDirectory = sourceDirectory.trim()
    }
  }

  /**
   * Set the output directory where the maven artifacts should be copied to. <br/>
   * Default value is: "./dependencies"
   *
   * @param outputDirectory The output location.
   */
  void setOutputDirectory(String outputDirectory) {
    if (outputDirectory != null && !outputDirectory.isBlank()) {
      this.outputDirectory = outputDirectory.trim()
    }
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def body = {

      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Starting download of maven artifacts")
      runBeforeScripts(config, globals)

      def downloadMavenArtifacts = this.mavenArtifacts

      if (this.sourceDirectory.length() > 0) {
        steps.echo("Reading additional information from files in ${this.sourceDirectory}")
        steps.dir(this.sourceDirectory) {
          def files = steps.findFiles(glob: '**/*.xml')
          files.each { file ->
            String filePath = file.getPath()
            steps.echo("Try to read pom info from: ${filePath}")
            def pomInfo = steps.readMavenPom(file: filePath)
            steps.echo("Found pom info: ${pomInfo}")
            String groupId = pomInfo.getGroupId()
            String artifactId = pomInfo.getArtifactId()
            String version = pomInfo.getVersion()
            // desired format: groupId:artifactId:version[:packaging]
            String mvnArtifact = "${groupId}:${artifactId}:${version}"
            String packaging = pomInfo.getPackaging()
            if (packaging != null && !packaging.isBlank()) {
              mvnArtifact += ":${packaging}"
            }
            String fileName = file.getName()
            downloadMavenArtifacts.put(fileName, mvnArtifact)
          }
        }
      }

      steps.echo("Preparing maven commands for artifact download")
      def mavenGoal = "dependency:copy"
      def outputDirectory = this.outputDirectory
      def allMavenCommands = ""
      def downloadInfo = ""
      downloadMavenArtifacts.each { key, value ->
        String mavenCommand = "mvn ${mavenGoal} -Dmdep.useBaseVersion -Dartifact=${value} -DoutputDirectory=${outputDirectory}"
        allMavenCommands += """
          ${mavenCommand};"""
        def fileName = parseArtifactStringToFileName(value)
        downloadInfo += downloadInfo.isBlank() ? "" : ","
        downloadInfo += "${key}=${fileName}"
      }

      // step execution
      if (!skipStepExecution) {
        steps.echo("Executing maven command(s): ${allMavenCommands}")
        steps.sh(script: allMavenCommands)
        steps.echo("Exporting global environment variable: BI_MAVEN_DEPENDENCY_COPY_INFO=${downloadInfo}")
        (globals.env['BI_MAVEN_DEPENDENCY_COPY_INFO'] = downloadInfo)
      }
      runAfterScripts(config, globals)
    }

    // if maven settings is configured, wrap this in "withMaven"
    if (config.mavenSettingsId) {
      def innerBody = body
      body = {
        steps.withMaven(mavenSettingsConfig: config.mavenSettingsId, mavenOpts: "-Dorg.slf4j.simpleLogger.defaultLogLevel=${mavenLogLevel}") {
          innerBody()
        }
      }
    }

    steps.stage('Download maven artifacts') {
      steps.container(stepContainerName()) {
        body()
      }
    }
  }

  @Override
  String stepContainerName() {
    return "maven-download"
  }

  @Override
  String stepContainerImageName(config) {
    return "maven:3.8.1-openjdk-11-slim"
  }

  @Override
  def createContainerTemplate(config, steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(config),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )
  }

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*              helper methods              */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  private static String parseArtifactStringToFileName(String artifact) {
    def artifactStrings = artifact.split(":")
    def artifactId = ""
    def version = ""
    def packaging = ""
    if (artifactStrings.size() >= 3) {
      // groupId = artifactStrings[0]
      artifactId = artifactStrings[1]
      version = artifactStrings[2]
    }
    if (artifactStrings.size() >= 4) {
      packaging = artifactStrings[3]
    }
    //default maven artifact naming
    if (packaging.isBlank()) {
      return "${artifactId}-${version}.jar"
    }
    return "${artifactId}-${version}.${packaging}"
  }

}
