package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute npm commands.
 */
@InheritConstructors
@SuppressWarnings('unused')
class NpmStepModel extends AbstractStepModel {

  boolean skipStepExecution = false
  String[] shellCommands = []
  String artifactPattern = null

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Sets the NPM commands to execute.
   * @param shellCommands The NPM commands to execute.
   */
  void setShellCommands(String[] shellCommands = []) {
    this.shellCommands = shellCommands
  }

  /**
   * Set an (optional) ant pattern to archive them as an artifacts.
   * @param artifactPattern
   */
  void setArtifactPattern(String artifactPattern) {
    this.artifactPattern = artifactPattern;
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def body = {
      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      def npmBuildCommand = ""

      if (shellCommands.length > 0) {
        shellCommands.each { command ->
          npmBuildCommand += """
             ${command}
          """
        }
      }

      runBeforeScripts(config, globals)
      if (!skipStepExecution) {
        steps.sh(npmBuildCommand)
        if (artifactPattern || artifactPattern.trim().length() > 0) {
          steps.archiveArtifacts(
            artifacts: artifactPattern,
            onlyIfSuccessful: true,
            fingerprint: true
          )
        }
      }
      runAfterScripts(config, globals)
    }

    steps.stage('Npm Build') {
      steps.container(stepContainerName()) {
        body()
      }
    }
  }

  @Override
  String stepContainerName() {
    return "npm"
  }

  @Override
  String stepContainerImageName(config) {
    return "node:slim"
  }

  @Override
  def createContainerTemplate(config, steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(config),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )
  }
}
