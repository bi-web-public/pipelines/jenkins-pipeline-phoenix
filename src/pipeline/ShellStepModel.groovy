package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute shell commands.
 */
@InheritConstructors
@SuppressWarnings('unused')
class ShellStepModel extends AbstractStepModel {

  String[] shellCommands = null

  /**
   * Shell commands that should be executed. Every object in the passed list is a separate command.
   * Example: setShellCommands("ls -al", "cd dirname", "ls -al")
   * @param shellCommands shell commands to execute
   */
  void setShellCommands(String... shellCommands) {
    this.shellCommands = shellCommands
  }

  /**
   * Amend a shell commands that should be executed.
   * Example: addShellCommand("ls -al")
   * @param shellCommand shell command to execute
   */
  void addShellCommand(String shellCommand) {
    if (shellCommand == null || shellCommand.isBlank()) {
      return
    }
    List<String> commands = []
    if (this.shellCommands != null && this.shellCommands.length > 0) {
      commands.addAll(shellCommands)
    }
    commands.add(shellCommand)
    this.shellCommands = commands.toArray()
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def allCommands = ""
    def steps = globals.steps

    def body = {

      steps.echo("Executing shell commands")

      runBeforeScripts(config, globals)

      if (shellCommands && shellCommands.length > 0) {
        shellCommands.each { command ->
          steps.echo "shell commmand: ${command}"
          allCommands += """ 
              ${command} ; 
            """
        }
        printf("allCommands=${allCommands}")

        steps.sh(allCommands)

      } else steps.echo("no shell commands defined")

      runAfterScripts(config, globals)
    }

    steps.stage('Shell Execution') {
      steps.container(stepContainerName()) {
        body()
      }
    }

  }

  @Override
  String stepContainerName() {
    return "shell"
  }

  @Override
  String stepContainerImageName(config) {
    return "brandography/alpine-zip"
  }

  @Override
  def createContainerTemplate(config, steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(config),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )

  }
}
