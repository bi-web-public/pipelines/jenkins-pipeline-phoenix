package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute maven legacy builds.
 * @deprecated This class is deprecated, use {@link MavenJava8StepModel} instead.
 */
@Deprecated
@InheritConstructors
@SuppressWarnings('unused')
class MavenSonarQubePrepareStepModel extends AbstractStepModel {

  boolean skipStepExecution = false
  boolean skipTests = false
  boolean appendBranchToVersion = true
  String modulesToIgnore = ""
  List appendBranchToVersionExceptBranches = ["master"]
  String extraOpts = ""
  List mavenReleaseBranches = []

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Sets a flag which indicates, if the test should be skipped.
   * @param skipTests True if the test should be skipped. False otherwise.
   */
  void skipTests(boolean skipTests = true) {
    this.skipTests = skipTests
  }

  /**
   * Appends the branch to the version.
   * @param appendBranchToVersion True, if the branch should be appended.
   * @param appendBranchToVersionExceptBranches Branches that should not be appended, even if the feature toggle is
   * set to true.
   */
  void appendBranchToVersion(boolean appendBranchToVersion = true, String... appendBranchToVersionExceptBranches) {
    this.appendBranchToVersion = appendBranchToVersion
    if (appendBranchToVersionExceptBranches.length == 0) {
      this.appendBranchToVersionExceptBranches = ["master"]
    } else {
      this.appendBranchToVersionExceptBranches = appendBranchToVersionExceptBranches as List
    }
  }

  /**
   * Defines options, that should be added to the maven execution command.
   * @param extraOpts The options to add.
   */
  void options(String extraOpts = "") {
    this.extraOpts = extraOpts
  }

  /**
   * Enables release for a passed set of release branch names.
   * @param mavenReleaseBranches The
   */
  void enableReleases(String... mavenReleaseBranches) {
    if (mavenReleaseBranches.length == 0) {
      this.mavenReleaseBranches = ["master"]
    } else {
      this.mavenReleaseBranches = mavenReleaseBranches as List
    }
  }

  /**
   * Sets the modules that should be ignored during build phase. For example "!documentation"
   * @param modulesToIgnore The modules to ignore.
   */
  void setModulesToIgnore(String modulesToIgnore) {
    this.modulesToIgnore = modulesToIgnore
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps


    def body = {

      def pomInfo = steps.readMavenPom(file: 'pom.xml')
      vars['MAVEN_GROUP'] = pomInfo.groupId
      vars['MAVEN_ARTIFACT'] = pomInfo.artifactId
      vars['MAVEN_VERSION'] = pomInfo.version

      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Starting maven build")

      if ((globals.gitInfo.name in this.mavenReleaseBranches) && !globals.gitInfo.isTag) {
        config.booleanParameter("MAVEN_RELEASE", "Release a new maven version", false)
        config.stringParameter("NEW_MAVEN_VERSION", "Version to use for release", pomInfo.version.replace("-SNAPSHOT", ""))
      }

      def goal = "package dependency:copy-dependencies"

      this.modulesToIgnore = modulesToIgnore.length() > 0 ? "-pl '${modulesToIgnore}'" : ""

      def mavenCommand = """
					export _JAVA_OPTIONS=-Djdk.net.URLClassPath.disableClassPathURLCheck=true
					export DOCKER_HOST=127.0.0.1
					[ -z "\${DOCKER_CONFIG}" ] || ( rm -rf ~/.docker; ln -s \${DOCKER_CONFIG} ~/.docker )
					mvn ${
        modulesToIgnore
      } -B -DargLine='-Djava.security.egd=file:///dev/urandom' -Dmaven.test.failure.ignore=true -Dmaven.test.skip=${
        skipTests
      } ${extraOpts} ${goal}
				"""

      def mavenVersion
      if (globals.gitInfo.isTag || !appendBranchToVersion || (globals.gitInfo.name in this.appendBranchToVersionExceptBranches)) {
        mavenVersion = pomInfo.version
      } else {
        mavenVersion = pomInfo.version.replace("-SNAPSHOT", "") + "-" + globals.gitInfo.name.replace("/", "-") + "-SNAPSHOT"
        mavenCommand = """
						mvn versions:set -DnewVersion='${mavenVersion}'
						${mavenCommand}
					"""
      }
      globals.currentBuild.description = mavenVersion
      runBeforeScripts(config, globals)
      if (!skipStepExecution) steps.sh(mavenCommand)
      runAfterScripts(config, globals)
    }

    // if maven settings is configured, wrap this in "withMaven"
    if (config.mavenSettingsId) {
      def innerBody = body
      body = {
        steps.withMaven(mavenSettingsConfig: config.mavenSettingsId) {
          innerBody()
        }
      }
    }

    steps.stage('Maven SonarQube Prepare') {
      steps.container(stepContainerName()) {
        body()
      }
    }
  }

  @Override
  String stepContainerName() {
    return "maven-sonarqube-prepare"
  }

  @Override
  String stepContainerImageName(config) {
    return "bihub/maven-git:3.8.1-openjdk-8-slim"
  }

  @Override
  def createContainerTemplate(config, steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(config),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )

  }
}
