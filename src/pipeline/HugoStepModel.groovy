package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute hugo builds.
 */
@InheritConstructors
@SuppressWarnings('unused')
class HugoStepModel extends AbstractStepModel {

  boolean skipStepExecution = false
  String extraOpts = ""

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Defines options, that should be added to the execution command.
   * @param extraOpts The options to add.
   */
  void options(String extraOpts = "") {
    this.extraOpts = extraOpts
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def body = {

      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Starting hugo build")

      def hugoCommand = """ hugo ${extraOpts} """

      runBeforeScripts(config, globals)
      if (!skipStepExecution) {
        steps.echo("Executing hugo build command: " + hugoCommand)
        steps.sh(hugoCommand)
      }
      runAfterScripts(config, globals)
    }

    steps.stage('Hugo Build') {
      steps.container(stepContainerName()) {
        body()
      }
    }

  }

  @Override
  String stepContainerName() {
    return "hugo"
  }

  @Override
  String stepContainerImageName(config) {
    return "klakegg/hugo:0.80.0"
  }

  @Override
  def createContainerTemplate(config, steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(config),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )
  }

}
