package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to expose global variables during build time.
 */
@InheritConstructors
@SuppressWarnings('unused')
class ExposeGlobalEnvironmentVariablesStepModel extends AbstractStepModel {

  boolean skipStepExecution = false
  List<EnvVarInfo> envVarInfoList = []
  def steps

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Expose an environment variable with the given name and value.
   *
   * @param name The variable name.
   * @param value The variable value.
   * @param overwriteIfPresent Indicates if the variable should be overwritten.
   */
  void expose(String name, String value, boolean overwriteIfPresent) {
    if (name != null && !name.isBlank()) {
      def preparedValue = value != null ? value.trim() : ""
      envVarInfoList.add(new EnvVarInfo(name.trim(), preparedValue, "", "", overwriteIfPresent))
    }
  }

  /**
   * Expose an environment variable with the given name and value.
   *
   * @param name The variable name.
   * @param value The variable value.
   */
  void expose(String name, String value) {
    expose(name, value, false)
  }

  /**
   * Expose environment variables from a given file.
   * Assuming a list of "name=value" pairs.
   *
   * @param filePath The path to the file containing variables.
   * @param overwriteIfPresent Indicates if the variables should be overwritten.
   */
  void exposeFromFile(String filePath, boolean overwriteIfPresent) {
    if (filePath != null && !filePath.isBlank()) {
      envVarInfoList.add(new EnvVarInfo("", "", filePath.trim(), "", overwriteIfPresent))
    }
  }

  /**
   * Expose environment variables from a given file.
   * Assuming a list of "name=value" pairs.
   *
   * @param filePath The path to the file containing variables.
   */
  void exposeFromFile(String filePath) {
    exposeFromFile(filePath, false)
  }

  /**
   * Expose an environment variable by the return value of a shell script.
   * Assuming a "name=value" pair.
   *
   * @param shellScript The shell script to be executed.
   * @param overwriteIfPresent Indicates if the variables should be overwritten.
   */
  void exposeFromScriptResult(String shellScript, boolean overwriteIfPresent) {
    if (shellScript != null && !shellScript.isBlank()) {
      envVarInfoList.add(new EnvVarInfo("", "", "", shellScript.trim(), overwriteIfPresent))
    }
  }

  /**
   * Expose an environment variable by the return value of a shell script.
   * Assuming a "name=value" pair.
   *
   * @param shellScript The shell script to be executed.
   */
  void exposeFromScriptResult(String shellScript) {
    exposeFromScriptResult(shellScript, false)
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    this.steps = globals.steps

    def body = {

      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      steps.echo("Start exposing global variables")
      runBeforeScripts(config, globals)

      List<EnvironmentVariable> exposeEnvVars = []
      envVarInfoList.each { envVarInfo ->
        steps.echo("Extracting information from: " + envVarInfo.toString())
        getEnvironmentVariables(envVarInfo).each { envVar ->
          steps.echo("Found environment variable to be exposed: " + envVar.toString())
          exposeEnvVars.add(envVar)
        }
      }

      // step execution
      if (!skipStepExecution) {
        exposeEnvVars.each { envVar ->
          steps.echo("Exposing environment variable: " + envVar.toString())
          // check env var already exists
          if (globals.env[envVar.name]) {
            steps.echo("Environment variable already exists, having the value: ${globals.env[envVar.name]}")
            if (envVar.overwrite) {
              steps.echo("Overwriting environment variable with: " + envVar.toString())
            } else {
              // abort the build
              String errMsg = "Cannot overwrite existing environment variable with: '" + envVar.toString() + "' Overwrite was set to 'false'!"
              steps.echo(errMsg) // print the error information also to the build log
              throw new RuntimeException(errMsg)
            }
          }
          // finally set the env var
          (globals.env[envVar.name] = envVar.value)
        }
      }
      runAfterScripts(config, globals)
    }

    steps.stage('Expose global variables') {
      steps.container(stepContainerName()) {
        body()
      }
    }
  }

  @Override
  String stepContainerName() {
    return "expose-global-variables"
  }

  @Override
  String stepContainerImageName(config) {
    return "debian:buster-slim"
  }

  @Override
  def createContainerTemplate(config, steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(config),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false,
      runAsUser: '1000',
      runAsGroup: '1000'
    )
  }

  /**
   * Environment variable info holder class.
   */
  private class EnvVarInfo {

    String name
    String value
    String filePath
    String shellScript
    boolean overwrite

    EnvVarInfo(String name, String value, String filePath, String shellScript, boolean overwrite) {
      this.name = name
      this.value = value
      this.filePath = filePath
      this.shellScript = shellScript
      this.overwrite = overwrite
    }

    /**
     * Returns all information as string.
     * @return A string with all information.
     */
    String toString() {
      return "EnvVarInfo[name=${name},value=${value},filePath=${filePath},shellScript=${shellScript},overwrite=${overwrite}]"
    }
  }

  /**
   * The environment variable class.
   */
  private class EnvironmentVariable {

    String name
    String value
    boolean overwrite

    EnvironmentVariable(String name, String value, boolean overwrite) {
      this.name = name.trim()
      this.value = value.trim()
      this.overwrite = overwrite
    }

    /**
     * Returns all information as string.
     * @return A string with all information.
     */
    String toString() {
      return "EnvironmentVariable[name=${name},value=${value},overwrite=${overwrite}]"
    }
  }

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*              helper methods              */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  /**
   * Creates a list of environment variables from the given EnvVarEntry.
   *
   * @param envVarInfo The EnvVarEntry.
   * @return A List of EnvironmentVariables, or an empty List if no variables could be extracted.
   */
  private List<EnvironmentVariable> getEnvironmentVariables(EnvVarInfo envVarInfo) {
    def environmentVariables = []
    if (envVarInfo == null) {
      return environmentVariables
    }
    //first check file
    getEnvironmentVariablesByFile(envVarInfo.filePath, envVarInfo.overwrite).each { envVar -> environmentVariables.add(envVar) }

    //than check script
    getEnvironmentVariablesByScript(envVarInfo.shellScript, envVarInfo.overwrite).each { envVar -> environmentVariables.add(envVar) }

    //last check name,value
    if (envVarInfo.name != null && !envVarInfo.name.isBlank()) {
      environmentVariables.add(new EnvironmentVariable(envVarInfo.name, envVarInfo.value, envVarInfo.overwrite))
    }
    return environmentVariables
  }

  /**
   * Creates a list of environment variables from the content of the file at the given path.
   * Each line of the files content will be interpreted as a separate variable.
   *
   * @param filePath The path to a file containing variables.
   * @return A List of EnvironmentVariables, or an empty List if no variables could be extracted.
   */
  private List<EnvironmentVariable> getEnvironmentVariablesByFile(String filePath, boolean overwrite) {
    if (filePath == null || filePath.isBlank()) {
      return Collections.emptyList()
    }
    // expand env vars
    String filePathExpansionCommand = "echo ${filePath}"
    String preparedFilePathExpansionCommand = """
          ${filePathExpansionCommand};"""
    String expandedPath = steps.sh(script: preparedFilePathExpansionCommand, returnStdout: true).trim()
    String fileContent = steps.readFile(expandedPath)
    return getEnvironmentVariables(fileContent, overwrite)
  }

  /**
   * Creates a list of environment variables from the result of a given shell script.
   * Each line of the scripts result will be interpreted as a separate variable.
   *
   * @param shellScript The script to be executed.
   * @return A List of EnvironmentVariables, or an empty List if no variables could be extracted.
   */
  private List<EnvironmentVariable> getEnvironmentVariablesByScript(String shellScript, boolean overwrite) {
    if (shellScript == null || shellScript.isBlank()) {
      return Collections.emptyList()
    }
    String preparedScript = """
      ${shellScript};"""
    String result = steps.sh(script: preparedScript, returnStdout: true).trim()
    return getEnvironmentVariables(result, overwrite)
  }

  /**
   * Extracts environment variables from a given string.
   * Each line of the string will be interpreted as a single variable.
   *
   * @param multiLine A string containing the environment variables (one per line).
   * @return A List of EnvironmentVariables, or an empty List if no variables could be extracted.
   */
  private List<EnvironmentVariable> getEnvironmentVariables(String multiLine, boolean overwrite) {
    def envVarList = []
    if (multiLine == null) {
      return envVarList
    }
    def envVarsStrings = multiLine.split(System.lineSeparator())
    envVarsStrings.each { envVarsString ->
      def envVar = getEnvironmentVariable(envVarsString, overwrite)
      if (envVar != null) {
        envVarList.add(envVar)
      }
    }
    return envVarList
  }

  /**
   * Extracts an environment variable from a string.
   * The string must contain only a single variable.
   *
   * @param singleLine A string containing a single variable.
   * @return An Optional of the EnvironmentVariable, or an empty Optional if the input did not contain at least a name for the variable.
   */
  private EnvironmentVariable getEnvironmentVariable(String singleLine, boolean overwrite) {
    if (singleLine == null) {
      return null
    }
    def keyValueArray = singleLine.split("=", 2)
    def key = keyValueArray[0]
    if (key.isBlank()) {
      return null
    }
    def value = keyValueArray.size() > 1 ? keyValueArray[1] : ""
    return new EnvironmentVariable(key, value, overwrite)
  }
}
