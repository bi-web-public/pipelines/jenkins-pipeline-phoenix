package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute maven builds based on Java 17.
 */
@InheritConstructors
@SuppressWarnings('unused')
class MavenJava17StepModel extends AbstractStepModel {

  private boolean deploy = false
  private boolean skipStepExecution = false
  private boolean skipTests = false
  private boolean ignoreTestFailures = false
  private boolean appendBranchToVersion = true
  private String modulesToIgnore = ""
  private List appendBranchToVersionExceptBranches = ["main"]
  private String extraOpts = ""
  private List mavenReleaseBranches = []
  private Map<String, String> secrets = [:]

  private final boolean prepareRelease
  private final boolean performRelease

    MavenJava17StepModel(Closure bodyClosure, Map vars, boolean prepareRelease, boolean performRelease) {
    super(bodyClosure, vars)
    this.prepareRelease = prepareRelease
    this.performRelease = performRelease
  }

  /**
   * Expose an environment variable containing a secret defined in jenkins.
   * @param environmentVariable
   * @param secretId
   */
  void useSecret(String environmentVariable, String secretId) {
    if (environmentVariable != null && !environmentVariable.isBlank() && secretId != null && !secretId.isBlank()) {
      this.secrets[environmentVariable] = secretId
    }
  }

  /**
   * Decides, if the the build should be deployed.
   * @param deploy True if the build should be deployed. False otherwise.
   */
  void deploy(boolean deploy = true) {
    this.deploy = deploy
  }

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Sets a flag which indicates, if the test should be skipped.
   * @param skipTests True if the test should be skipped. False otherwise.
   */
  void skipTests(boolean skipTests = true) {
    this.skipTests = skipTests
  }

  /**
   * Sets a flag which indicates, if test failures should be ignored.
   * @param ignoreTestFailures True if test failures should be ignored. False otherwise.
   */
  void ignoreTestFailures(boolean ignoreTestFailures = true) {
    this.ignoreTestFailures = ignoreTestFailures
  }

  /**
   * Appends the branch to the version.
   * @param appendBranchToVersion True, if the branch should be appended.
   * @param appendBranchToVersionExceptBranches Branches that should not be appended, even if the feature toggle is
   * set to true.
   */
  void appendBranchToVersion(boolean appendBranchToVersion = true, String... appendBranchToVersionExceptBranches) {
    this.appendBranchToVersion = appendBranchToVersion
    if (appendBranchToVersionExceptBranches.length == 0) {
      this.appendBranchToVersionExceptBranches = ["main"]
    } else {
      this.appendBranchToVersionExceptBranches = appendBranchToVersionExceptBranches as List
    }
  }

  /**
   * Defines options, that should be added to the maven execution command.
   * @param extraOpts The options to add.
   */
  void options(String extraOpts = "") {
    if (extraOpts != null && !extraOpts.isBlank()) {
      if (this.extraOpts.isBlank()) {
        this.extraOpts = extraOpts
      } else {
        this.extraOpts += " " + extraOpts
      }
    }
  }

  /**
   * Enables release for a passed set of release branch names.
   * @param mavenReleaseBranches The maven release branches.
   */
  void enableReleases(String... mavenReleaseBranches) {
    if (mavenReleaseBranches.length == 0) {
      this.mavenReleaseBranches = ["main"]
    } else {
      this.mavenReleaseBranches = mavenReleaseBranches as List
    }
  }

  /**
   * Sets the modules that should be ignored during build phase. For example "!documentation"
   * @param modulesToIgnore The modules to ignore.
   */
  void setModulesToIgnore(String modulesToIgnore) {
    this.modulesToIgnore = modulesToIgnore
  }

  /**
   * Calculate the next development version for a given version.
   * @param version the current version
   * @return the next development version
   */
  static def nextDevelopmentVersion(version) {
    def versionParts = version.toString().split("-")
    def numberParts = versionParts[0].split("\\.")
    def nextVersion = numberParts.last().toInteger() + 1
    numberParts[numberParts.length - 1] = nextVersion
    versionParts[0] = numberParts.join('.')
    versionParts.join('-')
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def body = {

      def pomInfo = steps.readMavenPom(file: 'pom.xml')
      vars['MAVEN_GROUP'] = pomInfo.groupId
      vars['MAVEN_ARTIFACT'] = pomInfo.artifactId
      vars['MAVEN_VERSION'] = pomInfo.version

      if (config.isCleanUpRequested) {
        steps.echo("Clean up, skipping step")
        return
      }

      if ((globals.gitInfo.branch in this.mavenReleaseBranches) && this.performRelease && !globals.gitInfo.isTag) {
        config.removeParameter("REQUEST_CLEANUP")
        config.booleanParameter("MAVEN_RELEASE", "Perform Maven Release", false)
        config.stringParameter("MAVEN_RELEASE_VERSION", "Release Version", pomInfo.version.replace("-SNAPSHOT", ""))
        config.stringParameter("NEXT_MAVEN_DEVELOPMENT_VERSION", "Next Development Version", nextDevelopmentVersion(pomInfo.version))
      }

      vars['MAVEN_RELEASE'] = globals.env.MAVEN_RELEASE
      vars['MAVEN_RELEASE_VERSION'] = globals.env.MAVEN_RELEASE_VERSION

      if (globals.env.MAVEN_RELEASE && globals.env.MAVEN_RELEASE == "true") {

        if (this.prepareRelease) {

          steps.echo("Preparing maven release")
          doPrepareRelease(globals, steps)

        } else if (this.performRelease) {

          steps.echo("Performing maven release")
          doPerformRelease(globals, steps)

        }

      } else if (!this.performRelease) {

        steps.echo("Starting maven build")
        doBuild(config, globals, steps, pomInfo)

      }
    }

    // if maven settings is configured, wrap this in "withMaven"
    if (config.mavenSettingsId) {
      def innerBody = body
      body = {
        steps.withMaven(mavenSettingsConfig: config.mavenSettingsId) {
          innerBody()
        }
      }
    }

    if (!secrets.isEmpty()) {
      secrets.each { entry ->
        def innerBody = body
        String environmentVariable = entry.getKey()
        String secretId = entry.getValue()
        body = {
          steps.withCredentials([steps.string(credentialsId: secretId, variable: environmentVariable)]) {
            innerBody()
          }
        }
      }
    }

    steps.stage(this.performRelease ? 'Maven Release' : 'Maven Build') {
      steps.container(stepContainerName()) {
        body()
      }
    }
  }

  void doPrepareRelease(Map globals, steps) {

    def releaseVersion = "${globals.env.MAVEN_RELEASE_VERSION}"
    if (releaseVersion.isBlank()) {
      steps.error("Missing release version during maven release.")
    }
    def developmentVersion = "${globals.env.NEXT_MAVEN_DEVELOPMENT_VERSION}"
    if (developmentVersion.isBlank()) {
      steps.error("Missing next development version during maven release.")
    }

    globals.currentBuild.description = "Prepare release of ${releaseVersion}"

    // prepare release
    def goal = "release:prepare -DreleaseVersion=${releaseVersion} -DdevelopmentVersion=${developmentVersion} " +
      "-DremoteTagging=false -DpushChanges=false dependency:copy-dependencies"
    def mavenCommand = """
      export _JAVA_OPTIONS=-Djdk.net.URLClassPath.disableClassPathURLCheck=true
      export DOCKER_HOST=127.0.0.1
      [ -z "\${DOCKER_CONFIG}" ] || ( rm -rf ~/.docker; ln -s \${DOCKER_CONFIG} ~/.docker )
      mvn -B -DargLine='-Djava.security.egd=file:///dev/urandom' -Darguments="${extraOpts}" ${goal}
    """
    steps.sh(mavenCommand)

    // push branch
    if (globals.gitRemoteUrl.contains("https://")) {
      steps.withCredentials([
        steps.usernamePassword(
          credentialsId: globals.gitCredentialsId,
          passwordVariable: 'GIT_PASS',
          usernameVariable: 'GIT_USER')
      ]) {
        def remoteUrl = globals.gitRemoteUrl.replace("https://", "")
        // both vars are masked by 'withCredentials'
        steps.sh("git push https://\$GIT_USER:\$GIT_PASS@${remoteUrl} ${globals.gitInfo.branch}")
      }
    }
  }

  void doPerformRelease(Map globals, steps) {

    def releaseVersion = "${globals.env.MAVEN_RELEASE_VERSION}"
    if (releaseVersion.isBlank()) {
      steps.error("Missing release version during maven release.")
    }

    globals.currentBuild.description = "Perform release of ${releaseVersion}"

    // push tag
    if (globals.gitRemoteUrl.contains("https://")) {
      steps.withCredentials([
        steps.usernamePassword(
          credentialsId: globals.gitCredentialsId,
          passwordVariable: 'GIT_PASS',
          usernameVariable: 'GIT_USER')
      ]) {
        def remoteUrl = globals.gitRemoteUrl.replace("https://", "")
        // both vars are masked by 'withCredentials'
        steps.sh("git push https://\$GIT_USER:\$GIT_PASS@${remoteUrl} \$(cat release.properties | grep scm.tag= | cut -d= -f2)")
      }
    }

    // perform release
    def goal = "release:perform -DlocalCheckout=true"
    def mavenCommand = """
      export _JAVA_OPTIONS=-Djdk.net.URLClassPath.disableClassPathURLCheck=true
      export DOCKER_HOST=127.0.0.1
      [ -z "\${DOCKER_CONFIG}" ] || ( rm -rf ~/.docker; ln -s \${DOCKER_CONFIG} ~/.docker )
      mvn -B -DargLine='-Djava.security.egd=file:///dev/urandom' ${goal}
    """
    steps.sh(mavenCommand)
  }

  void doBuild(config, Map globals, steps, pomInfo) {

    def goal = deploy ? "deploy dependency:copy-dependencies -DuniqueVersion=false" : "verify dependency:copy-dependencies"

    this.modulesToIgnore = modulesToIgnore.length() > 0 ? "-pl '${modulesToIgnore}'" : ""

    def mavenCommand = """
      export _JAVA_OPTIONS=-Djdk.net.URLClassPath.disableClassPathURLCheck=true
      export DOCKER_HOST=127.0.0.1
      [ -z "\${DOCKER_CONFIG}" ] || ( rm -rf ~/.docker; ln -s \${DOCKER_CONFIG} ~/.docker )
      mvn ${
      modulesToIgnore
    } -B -DargLine='-Djava.security.egd=file:///dev/urandom' -Dmaven.test.failure.ignore=${
      ignoreTestFailures
    } -Dmaven.test.skip=${
      skipTests
    } ${extraOpts} ${goal}
    """

    def mavenVersion
    if (globals.gitInfo.isTag || !appendBranchToVersion || (globals.gitInfo.branch in this.appendBranchToVersionExceptBranches)) {
      mavenVersion = pomInfo.version
    } else {
      mavenVersion = pomInfo.version.replace("-SNAPSHOT", "") + "-" + globals.gitInfo.name.replace("/", "-") + "-SNAPSHOT"
      mavenCommand = """
        mvn release:update-versions -DautoVersionSubmodules=true -DdevelopmentVersion=${mavenVersion}
        ${mavenCommand}
      """
    }
    globals.currentBuild.description = mavenVersion
    runBeforeScripts(config, globals)
    if (!skipStepExecution) steps.sh(mavenCommand)
    runAfterScripts(config, globals)
  }

  @Override
  String stepContainerName() {
    return "maven-java17"
  }

  @Override
  String stepContainerImageName(config) {
    // do not use slim image, because git is not included but necessary for at least the maven plugin "org.codehaus.mojo:buildnumber-maven-plugin"
    return "maven:3.8.5-openjdk-17-slim"
  }

  @Override
  def createContainerTemplate(config, steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(config),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )
  }
}
