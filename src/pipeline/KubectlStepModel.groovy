package pipeline

import groovy.transform.InheritConstructors

@InheritConstructors
@SuppressWarnings('unused')
class KubectlStepModel extends AbstractStepModel {

  def shellCommands = []
  boolean skipStepExecution = false

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  void addCommand(String command) {
    this.shellCommands.add(command)
  }

  @Override
  void doExecute(Object config, Map globals) {
    def steps = globals.steps

    def body = {
      runBeforeScripts(config, globals)
      if (!skipStepExecution) {
        steps.echo("Execute commands")
        shellCommands.each { command ->
          steps.sh(command)
        }
      }
      runAfterScripts(config, globals)
    }

    steps.stage('Kubectl command') {
      steps.container(stepContainerName()) {
        body()
      }
    }
  }

  @Override
  String stepContainerName() {
    return "kubectl"
  }

  @Override
  String stepContainerImageName(config) {
    return "bitnami/kubectl:1.18.6"
  }

  @Override
  def createContainerTemplate(config, steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(config),
      command: 'cat',
      ttyEnabled: true,
      // 1000 = default UID of jenkins user in default jnlp image
      runAsUser: '1000',
      alwaysPullImage: false
    )
  }
}
