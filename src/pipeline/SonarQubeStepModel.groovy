package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to execute SonarQube analyses and check against quality gates.
 */
@InheritConstructors
@SuppressWarnings('unused')
class SonarQubeStepModel extends AbstractStepModel {

  boolean skipStepExecution = false
  boolean ignoreQualityGateResult = false
  boolean enableGitLab = false

  // Important!
  // If you ever change a default value and/or introducing a new parameter/property with a default value,
  // keep in mind that almost all builds are affected! (trilith, ponti, synergy, mercato, phx-...),
  // So either all builds have to be adapted or a very common / not harming default value has to be chosen.
  // The most secure path to introduce something new is probably by choosing no default value at all
  // and simply ignore the parameter/property in the resulting command if it is not given.
  // Thus it is just an extension and won't break the existing build configurations

  String hostUrl = null
  String apiUrl = null
  String project = null
  String tokenReference = "sonarqube-token"
  String webhookSecretId = 'sonarqube-webhook-secret'
  String sourceDir = "src"
  String sourceFileEncoding = null
  String baseDir = "./"
  String binariesDir = "target/classes"
  String testBinariesDir = "target/test-classes"
  String testDir = "src/test"
  String exclusions = "src/main/resources/static/angular/**,**/node_modules/**,src/test/**,**/3rd-party/**,src/main/angular/**/e2e/**,src/main/angular/**/*.spec.ts"
  String librariesDir = "target/dependency"
  String jacocoReportPath = "target/site/jacoco/jacoco.xml"
  String projectVersion = null
  String gitLabProject = null
  String gitLabCommitSha = null
  String gitLabBranch = null
  String typescriptReportPaths = null
  String testInclusions = null
  String testExecutionReportPaths = null
  String junitReportPaths = null


  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Disables the quality gate.
   * Default: false.
   * @param disableQualityGate True, if the quality gate should be disabled.
   * @deprecated Use{@link #ignoreQualityGateResult()} instead. Will be removed soon.
   */
  @Deprecated
  void disableQualityGate(boolean disableQualityGate = true) {
    ignoreQualityGateResult(disableQualityGate)
  }

  /**
   * Ignores the quality gate result.
   * In consequence the pipeline will not stop if the criteria of the gate do not match.
   * Default: false.
   * @param ignoreQualityGateResult True, if the result of the quality gate check should be ignored.
   */
  void ignoreQualityGateResult(boolean ignoreQualityGateResult = true) {
    this.ignoreQualityGateResult = ignoreQualityGateResult
  }

  /**
   * Should be set to true, if the Gitlab integration should be enabled
   * @param gitLabProject Project ID in GitLab or internal id or namespace + name or namespace + path or url http or ssh url or url or web.
   * @param gitLabBranch The GitLab branch.
   * @param gitLabCommitSha The GitLab commit SHA.
   */
  void enableGitlab(gitLabProject, gitLabBranch, gitLabCommitSha) {
    this.enableGitLab = true
    if (!gitLabProject || gitLabProject.trim().length() == 0) {
      steps.error("Please provide a GitLab project URL.")
    }
    if (!gitLabBranch || gitLabBranch.trim().length() == 0) {
      steps.error("Please provide a GitLab commit branch.")
    }
    if (!gitLabCommitSha || gitLabCommitSha.trim().length() == 0) {
      steps.error("Please provide a GitLab commit SHA.")
    }
    this.gitLabProject = gitLabProject
    this.gitLabBranch = gitLabBranch
    this.gitLabCommitSha = gitLabCommitSha
  }

  /**
   * Sets the host url.
   * @param hostUrl The host url.
   */
  void setHostUrl(String hostUrl) {
    this.hostUrl = hostUrl
  }

  /**
   * Sets the project version
   * Default: null.
   * @param hostUrl The project version.
   */
  void setProjectVersion(String projectVersion) {
    this.projectVersion = projectVersion
  }

  /**
   * Sets the api url.
   * @param apiUrl The api url.
   */
  void setApiUrl(String apiUrl) {
    this.apiUrl = apiUrl
  }

  /**
   * Sets the name of the project.
   * @param project The name of the project.
   */
  void setProject(String project) {
    this.project = project
  }

  /**
   * The the name of the token in jenkins.
   * Default value: sonarqube-token
   * @param tokenReference The name of the jenkins token.
   */
  void setTokenReference(String tokenReference) {
    this.tokenReference = tokenReference
  }

  /**
   * The name of the webhook secret token in jenkins
   * Default value: sonarqube-webhook-secret
   * @param webhookSecretId The name of the jenkins token
   */
  void setWebhookSecretId(String webhookSecretId) {
    this.webhookSecretId = webhookSecretId
  }

  /**
   * Sets the src dir.
   * Default value: src
   * @param sourceDir The source directory.
   */
  void setSourceDir(String sourceDir) {
    this.sourceDir = sourceDir
  }

  /**
   * Sets the encoding for source files.
   * Default value: null - the default system encoding will be used
   * @param The encoding, e.g. "latin1"
   */
  void setSourceFileEncoding(String sourceFileEncoding) {
    this.sourceFileEncoding = sourceFileEncoding
  }

  /**
   * Sets the base directory.
   * Default value: ./
   * @param baseDir The base directory.
   */
  void setBaseDir(String baseDir) {
    this.baseDir = baseDir
  }

  /**
   * Sets the binaries directory.
   * Default value: target/classes
   * @param binariesDir The binaries directory.
   */
  void setBinariesDir(String binariesDir) {
    this.binariesDir = binariesDir
  }

  /**
   * Sets the test binaries directory.
   * Default value: target/classes
   * @param testBinariesDir The test binaries directory.
   */
  void setTestBinariesDir(String testBinariesDir) {
    this.testBinariesDir = testBinariesDir
  }

  /**
   * Sets the test directory.
   * Default value: src (but in conjunction with test.inclusions)
   * @param testDir The test directory.
   */
  void setTestDir(String testDir) {
    this.testDir = testDir
  }

  /**
   * Sets the libraries directory.
   * Default value: target/dependency
   * @param librariesDir The libraries directory.
   */
  void setLibrariesDir(String librariesDir) {
    this.librariesDir = librariesDir
  }

  /**
   * Sets the excluded files and folders.
   * Default value: Angular compile output, node_modules folder, src/test folder and 3rd-party folder.
   * @param exclusions The excluded files and folders.
   */
  void setExclusions(String exclusions) {
    this.exclusions = exclusions
  }

  /**
   * Sets the inclusion files and folders for tests.
   * Default value: Angular spec files, Java files in src/test
   * @param testInclusions The included files and folders.
   */
  void setTestInclusions(String testInclusions) {
    this.testInclusions = testInclusions
  }

  /**
   * Sets the paths for all reports with test execution information
   * @param testExecutionReportPaths
   */
  void setTestExecutionReportPath(String testExecutionReportPath) {
    this.testExecutionReportPaths = testExecutionReportPath
  }

  /**
   * Sets the paths for all reports with test execution information
   * @param testExecutionReportPaths
   */
  void setTestExecutionReportPaths(String testExecutionReportPaths) {
    this.testExecutionReportPaths = testExecutionReportPaths
  }

  /**
   * Sets the JaCoCo report path .
   * Default value: target/site/jacoco/jacoco.xml
   * @param jacocoReportPath The JaCoCo report path.
   */
  void setJacocoReportPath(String jacocoReportPath) {
    this.jacocoReportPath = jacocoReportPath
  }

  /**
   * Sets the JaCoCo report path .
   * Default value: target/site/jacoco/jacoco.xml
   * @param jacocoReportPath The JaCoCo report path.
   */
  void setJacocoReportPaths(String jacocoReportPaths) {
    this.jacocoReportPath = jacocoReportPaths
  }

  /**
   * Sets the sonar.junit.reportPaths property
   * @param junitReportPaths
   */
  void setJunitReportPath(String junitReportPath) {
    this.junitReportPaths = junitReportPath
  }

  /**
   * Sets the sonar.junit.reportPaths property
   * @param junitReportPaths
   */
  void setJunitReportPaths(String junitReportPaths) {
    this.junitReportPaths = junitReportPaths
  }

  /**
   * Sets the report paths for typescript coverage information
   * No default
   * @param typescriptReportPaths paths to coverage information
   */
  void setTypescriptReportPath(String typescriptReportPath) {
    this.typescriptReportPaths = typescriptReportPath
  }

  /**
   * Sets the report paths for typescript coverage information
   * No default
   * @param typescriptReportPaths paths to coverage information
   */
  void setTypescriptReportPaths(String typescriptReportPaths) {
    this.typescriptReportPaths = typescriptReportPaths
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def body = {

      steps.echo("Starting SonarQube scan")

      runBeforeScripts(config, globals)
      if (!skipStepExecution) {

        if (hostUrl == "http://sonarqube-sonarqube.sonarqube.svc.cluster.local:9000" || hostUrl == "https://sonarqube.phoenix.bi-web.de") {
          hostUrl = "https://sonarqube.bi-web.de"
        }

        if (apiUrl == "http://sonarqube-sonarqube.sonarqube.svc.cluster.local:9000" || apiUrl == "https://sonarqube.phoenix.bi-web.de" ) {
          apiUrl = "https://sonarqube.bi-web.de"
        }

        if (!hostUrl || hostUrl.trim().length() == 0) {
          steps.error("Host utl not set! Use 'setHostUrl(string url)' to specify the host url.")
        }
        if (!apiUrl || apiUrl.trim().length() == 0) {
          steps.error("API utl not set! Use 'setApiUrl(string url)' to specify the API url.")
        }
        if (!project || project.trim().length() == 0) {
          steps.error("Project name not set! Use 'setProject(string name)' to specify the project name.")
        }

        if (config.isCleanUpRequested) {
          steps.withCredentials([steps.string(credentialsId: tokenReference, variable: 'TOKEN')]) {
            // delete project
            steps.echo('''curl -u $TOKEN''' + """: -X POST ${apiUrl}/api/projects/delete?project=${project}""")
            steps.sh('''curl -u $TOKEN''' + """: -X POST ${apiUrl}/api/projects/delete?project=${project}""")
          }
        } else {

          // withSonarQubeEnv() adds additional properties and information to the execution/root context that are used
          // by waitForQualityGate() step to verify the quality gate result
          steps.withSonarQubeEnv() {
            steps.withCredentials([steps.string(credentialsId: tokenReference, variable: 'TOKEN')]) {
              def sonarQubeCommand = 'sonar-scanner'
              sonarQubeCommand += ' -Dsonar.login="$TOKEN"'
              sonarQubeCommand += " -Dsonar.host.url=${hostUrl}"
              sonarQubeCommand += " -Dsonar.projectKey=${project}"
              sonarQubeCommand += " -Dsonar.projectName=${project}"
              if (sourceDir && sourceDir.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.sources=${sourceDir}"
              }
              if (sourceFileEncoding && sourceFileEncoding.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.sourceEncoding=${sourceFileEncoding}"
              }
              if (baseDir && baseDir.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.projectBaseDir=${baseDir}"
              }
              if (binariesDir && binariesDir.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.java.binaries=${binariesDir}"
              }
              if (exclusions && exclusions.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.exclusions=${exclusions}"
              }
              if (typescriptReportPaths && typescriptReportPaths.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.typescript.lcov.reportPaths=${typescriptReportPaths}"
              }
              if (librariesDir && librariesDir.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.java.libraries=${librariesDir}"
              }
              if (testDir && testDir.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.tests=${testDir}"
              }
              if (jacocoReportPath && jacocoReportPath.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.coverage.jacoco.xmlReportPaths=${jacocoReportPath}"
              }
              if (testBinariesDir && testBinariesDir.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.java.test.binaries=${testBinariesDir}"
              }
              if (librariesDir && librariesDir.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.java.test.libraries=${librariesDir}"
              }
              if (testInclusions && testInclusions.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.test.inclusions=${testInclusions}"
              }
              if (testExecutionReportPaths && testExecutionReportPaths.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.testExecutionReportPaths=${testExecutionReportPaths}"
              }
              if (junitReportPaths && junitReportPaths.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.junit.reportPaths=${junitReportPaths}"
              }
              if (projectVersion && projectVersion.trim().length() > 0) {
                sonarQubeCommand += " -Dsonar.projectVersion=${projectVersion}"
              }
              if (enableGitLab) {
                sonarQubeCommand += " -Dsonar.gitlab.project_id=${gitLabProject}"
                sonarQubeCommand += " -Dsonar.gitlab.commit_sha=${gitLabCommitSha}"
                sonarQubeCommand += " -Dsonar.gitlab.ref_name=${gitLabBranch}"
                sonarQubeCommand += " -Dsonar.scm.provider=git"
                sonarQubeCommand += " -Dsonar.scm.disabled=false"
              } else {
                sonarQubeCommand += " -Dsonar.scm.disabled=true"
              }

              steps.sh(sonarQubeCommand)
            }
          }
          // the timeout is for "just in case" to fail the job after the given time
          steps.timeout(time: 1, unit: 'HOURS') {
            steps.echo("SonarQube waiting for quality gate result ...")
            // do not abort pipeline if the quality gate result should be ignored
            steps.waitForQualityGate(abortPipeline: !ignoreQualityGateResult, credentialsId: tokenReference, webhookSecretId: webhookSecretId)
          }
        }
      }

      runAfterScripts(config, globals)
    }

    steps.stage('SonarQube Scan') {
      steps.container(stepContainerName()) {
        body()
      }
    }

  }

  @Override
  String stepContainerName() {
    return "sonarqube-scanner"
  }

  @Override
  String stepContainerImageName(config) {
    if (config.isCleanUpRequested) {
      return "curlimages/curl:7.79.1"
    }
    return "sonarsource/sonar-scanner-cli:4.6"
  }

  @Override
  def createContainerTemplate(config, steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(config),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false,
      runAsUser: '1000',
      runAsGroup: '1000'
    )
  }
}
