package pipeline

import groovy.transform.InheritConstructors

/**
 * Build step to deploy docker images to a kubernetes cluster via helm and tiller.
 */
@InheritConstructors
@SuppressWarnings('unused')
class HelmStepModel extends AbstractStepModel {

  boolean debug = false
  boolean dryRun = false
  boolean skipValidation = true
  boolean skipStepExecution = false
  boolean withDeploymentDeleteFirst = false
  String chartFolderName = null
  String namespace = null
  String releaseName = null
  String helmVariables = ""
  String helmStringVariables = ""
  String valuesFile = ""
  String buildDescription = null
  Map<String, String> helmVariablesMap = [:]
  Map<String, String> helmStringVariablesMap = [:]
  int helmTimeout = 600

  /**
   * Decided if the helm debug mode should be enabled.
   * @param debug True, if the helm debug mode should be enabled. False otherwise.
   */
  void debug(boolean debug = true) {
    this.debug = debug
  }

  /**
   * Decided if the helm dry-run mode should be enabled.
   * @param dryRun True, if the helm dry-run mode should be enabled. False otherwise.
   */
  void dryRun(boolean dryRun = true) {
    this.dryRun = dryRun
  }

  /**
   * Decided if helm should ignore openAPI validation.
   * For backward compatibility the default value is set to "true".
   * @param skipValidation True, if validation should be skipped. False otherwise.
   */
  void skipValidation(boolean skipValidation = true) {
    this.skipValidation = skipValidation
  }

  /**
   * Sets a flag which indicates, if the step execution should be skipped.
   * All pre and after scripts are executed, even if the step execution is set to true.
   * @param skipStepExecution True if the test should be skipped. False otherwise.
   */
  void skipStepExecution(boolean skipStepExecution = true) {
    this.skipStepExecution = skipStepExecution
  }

  /**
   * Overwrites the default timeout of 600s (10 min) for the helm command.
   * @param helmTimeout Timeout in seconds.
   */
  void overwriteDefaultTimeout(int helmTimeout) {
    this.helmTimeout = helmTimeout
  }

  /**
   * Decided if helm deletes the release before deploying it
   * @param withDeploymentDeleteFirst True, if the helm clean up before deploying it release. False otherwise.
   */
  void withDeploymentDeleteFirst(boolean withDeploymentDeleteFirst = true) {
    this.withDeploymentDeleteFirst = withDeploymentDeleteFirst
  }

  /**
   * Sets the name of the chart folder from where the helm chart should be loaded. Supports paths.
   * @param chartFolderName The name of the chart folder.
   */
  void chartFolderName(String chartFolderName) {
    this.chartFolderName = chartFolderName
  }

  /**
   * Sets the kubernetes namespace of the deployment.
   * @param namespace The kubernetes namespace.
   */
  void namespace(String namespace) {
    this.namespace = namespace
  }

  /**
   * Adds a helm variable to overwrite values in helm chart definition.
   * @param key The key of the helm variable.
   * @param value The boolean value of the helm variable.
   */
  void addHelmVariable(String key, boolean value) {
    addHelmVariable(key, value ? "true" : "false")
  }

  /**
   * Adds a helm variable to overwrite values in helm chart definition.
   * @param key The key of the helm variable.
   * @param value The int value of the helm variable.
   */
  void addHelmVariable(String key, int value) {
    addHelmVariable(key, "" + value)
  }

  /**
   * Adds a helm variable to overwrite values in helm chart definition.
   * implementing a map secures correct variable overwriting within 'before'-cloisure
   * @param key The key of the helm variable.
   * @param value The string value of the helm variable.
   * @author: tgr
   */
  void addHelmVariable(String key, String value) {
    helmVariablesMap.put(key, value)
    helmVariables = "--set " + helmVariablesMap.collect { k, v -> "$k=$v" }.join(',')
  }

  /**
   * Adds a helm variable to overwrite values in helm chart definition where values are force to be interpreted as strings.
   * implementing a map secures correct variable overwriting within 'before'-cloisure
   * @param key The key of the helm variable.
   * @param value The string value of the helm variable.
   */
  void addHelmStringVariable(String key, String value) {
    helmStringVariablesMap.put(key, value)
    helmStringVariables = "--set-string " + helmStringVariablesMap.collect { k, v -> "$k=$v" }.join(',')
  }

  /**
   * Adds a values file to overwrite values in helm chart definition.
   * This file takes precedence over the values.yaml defined in the helm chart,
   * but will be overwritten by values provided with {@link #addHelmVariable(String, String)}
   *
   * @param filepath The path to the values.yaml
   * @see #addHelmVariable(String, String)
   */
  void addValuesFile(String filepath) {
    valuesFile = "-f " + filepath
  }

  /**
   * Sets the release name of the helm deployment.
   * @param releaseName The name of the helm deployment.
   */
  void releaseName(String releaseName) {
    if (releaseName != null) {
      releaseName = releaseName.replace("/", "-")
    }
    this.releaseName = releaseName
  }

  String releaseNameParameterName = null
  /**
   * TODO
   */
  void releaseNameFromParameter(String parameterName) {
    releaseNameParameterName = parameterName
  }


  /**
   * Sets a build description.
   * @param buildDescription The build description.
   */
  void buildDescription(String buildDescription) {
    this.buildDescription = buildDescription
  }

/**
 * Converts a given string to camelCase.
 * <p>The delimiters are used to identify the parts of the input string and will be discarded in the output.</p>
 * <p>
 *   e.g. text = "some-text" , delimiters = "-" -> return = "someText"
 *   e.g. text = "some-other_text" , delimiters = "-_" -> return = "someOtherText"
 * </p>
 * @param text The string to convert.
 * @param delimiter The delimiters.
 * @return The converted string.
 */
  static String toCamelCase(String text, String delimiters) {
    if (text == null || delimiters == null) {
      return text
    }
    def textParts = text.tokenize(delimiters)
    StringBuilder sb = new StringBuilder()
    for (textPart in textParts) {
      if (textPart.length() > 0) {
        sb.append(textPart.substring(0, 1).toUpperCase())
      }
      if (textPart.length() > 1) {
        sb.append(textPart.substring(1))
      }
    }
    String capitalizedCamelCase = sb.toString()
    if (capitalizedCamelCase.length() > 2) {
      return capitalizedCamelCase.substring(0, 1).toLowerCase() + capitalizedCamelCase.substring(1)
    } else {
      return capitalizedCamelCase.toLowerCase()
    }
  }

  /**
   * Executes the step.
   * @param config A passed configuration object.
   * @param globals A map with global variables.
   */
  void doExecute(config, Map globals) {

    def steps = globals.steps

    def chartFolderName = this.chartFolderName

    def debugParameter = "--debug"

    def dryRunParameter = "--dry-run"

    def skipValidationParameter = "--disable-openapi-validation"


    if (!chartFolderName || chartFolderName.trim().length() == 0) {
      steps.error("Chart folder name is not set!")
    }

    if (namespace == null || namespace.trim().length() == 0) {
      namespace = "default"
      steps.echo("Name space is not set. Using '${namespace}'")
    }

    if (!releaseName) {
      releaseName(globals.gitInfo.name)
      steps.echo("Release name  is not set. Using '${releaseName}' derived from git tag or branch")
    }

    if (!debug) {
      debugParameter = ""
    }

    if (!dryRun) {
      dryRunParameter = ""
    }

    if (!skipValidation) {
      skipValidationParameter = ""
    }

    if (globals.gitInfo != null && globals.gitInfo.commit != null) {
      addHelmVariable("git.commit", "${globals.gitInfo.commit.substring(0, 8)}-${globals.gitInfo.commit}")
    }

    def body = {

      steps.echo("Starting Kubernetes deployment")

      runBeforeScripts(config, globals)
      if (config.isCleanUpRequested || withDeploymentDeleteFirst) {
        steps.echo("Undeploying release ${releaseName} from namespace '${namespace}'")
        // delete the current release and ignore any error (e.g. release not available any more)
        steps.sh("helm uninstall '${releaseName}' -n '${namespace}' || true")
      }

      if (!config.isCleanUpRequested) {
        if (!skipStepExecution) {
          // substitue variables in values.yaml with env variables namespace and releaseName
          // not longer used so comment out for eventually later use
          // steps.echo("Substitute parameter in values.yaml. Result:")
          // String cmdEnvSubst = "apk add --update --no-cache gettext; export NAMESPACE=\"${namespace}\" RELEASE_NAME=\"${releaseName}\"; envsubst < ${chartFolderName}/values.yaml > ${chartFolderName}/values.yaml.out; mv ${chartFolderName}/values.yaml.out ${chartFolderName}/values.yaml; cat  ${chartFolderName}/values.yaml"
          // steps.sh(cmdEnvSubst)
          steps.echo("Deploying charts from folder '${chartFolderName}' as ${releaseName} to namespace '${namespace}'")
          steps.sh("helm upgrade --wait --timeout ${helmTimeout}s --install ${skipValidationParameter} ${debugParameter} ${dryRunParameter} ${helmVariables} ${helmStringVariables} ${valuesFile} --create-namespace --namespace '${namespace}' '${releaseName}' '${chartFolderName}' ")
        }
        if (buildDescription != null) {
          globals.currentBuild.description = buildDescription
        }
      }
      runAfterScripts(config, globals)
    }

    steps.stage('Kubernetes Deployment') {
      steps.container(stepContainerName()) {
        body()
      }
    }

  }

  @Override
  String stepContainerName() {
    return "helm"
  }

  @Override
  String stepContainerImageName(config) {
    return "alpine/helm:3.5.4"
  }

  @Override
  def createContainerTemplate(config, steps) {
    return steps.containerTemplate(
      name: stepContainerName(),
      image: stepContainerImageName(config),
      command: 'cat',
      ttyEnabled: true,
      alwaysPullImage: false
    )
  }
}
